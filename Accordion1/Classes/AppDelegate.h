//
//  AppDelegate.h
//  Accordion
//
//  Created by Enriquez Gutierrez Guillermo Ignacio on 8/27/10.
//  Copyright (c) 2010 Nacho4D.
//  See the file license.txt for copying permission.
//

#import <UIKit/UIKit.h>


@class N4AccordionViewController;
@class DetailViewController;

@interface AppDelegate : NSObject <UIApplicationDelegate> {
    
    UIWindow *window;
    
    N4AccordionViewController *rootViewController;
    DetailViewController *detailViewController;

}

@property (nonatomic, strong) IBOutlet UIWindow *window;

@property (nonatomic, strong) IBOutlet N4AccordionViewController *rootViewController;
@property (nonatomic, strong) IBOutlet DetailViewController *detailViewController;

@end
