//
//  File.m
//  MandalaChart
//
//  Created by Ignacio Enriquez Gutierrez on 8/9/10.
//  Copyright (c) 2010 Nacho4D.
//  See the file license.txt for copying permission.
//

#import "N4File.h"


@implementation N4File
@synthesize name, parentDirectory;
@synthesize expanded;
@synthesize level;

@synthesize fullName = _fullName;
@synthesize type = _type;
@synthesize creationDate = _creationDate;
@synthesize modificationDate = _modificationDate;

@synthesize image = _image;
@synthesize imageBig = _imageBig;

//Lazy properties:
- (NSString *) fullName
{
	return [parentDirectory stringByAppendingPathComponent:name];
}

- (UIImage *) image{
	if (!_image) {
		if ([NSFileTypeDirectory isEqualToString:[self type]]) 
			_image = [UIImage imageNamed:@"Folder56.png"];
		else
			_image = [UIImage imageNamed:@"Document56.png"];		
	}
	return _image;
}
- (UIImage *) imageBig{
	if (!_imageBig) {
		if ([NSFileTypeDirectory isEqualToString:[self type]]) 
			_imageBig = [UIImage imageNamed:@"Folder512.png"];
		else
			_imageBig = [UIImage imageNamed:@"Document512.png"];		
	}
	return _imageBig;
	
}
- (NSDate *) creationDate{
    
    if (!_creationDate)
    {
        NSFileManager *manager = [NSFileManager defaultManager];
        _creationDate = [[manager attributesOfItemAtPath:[parentDirectory stringByAppendingPathComponent:name] error:NULL]  fileCreationDate];
    }
	
    return _creationDate;
}
- (NSDate *) modificationDate{
    
    if (!_modificationDate)
    {
        NSFileManager *manager = [NSFileManager defaultManager];
        _modificationDate = [[manager attributesOfItemAtPath:[parentDirectory stringByAppendingPathComponent:name] error:NULL] fileModificationDate];
    }
	
	return _modificationDate;
	
}
- (NSString *) detailText{
	return [[self modificationDate] description];
}

- (NSString *) type{
	if (!_type)
    {
		NSFileManager *manager = [NSFileManager defaultManager];
		_type = [[manager attributesOfItemAtPath:[parentDirectory stringByAppendingPathComponent:name] 
											   error:NULL] fileType];
	}
	return _type;
}

- (BOOL) isDirectory{
	return	([[self type] isEqualToString:NSFileTypeDirectory]);
}
- (NSString *) description{
	return [NSString stringWithFormat:@"N4File:%@ directory:%@",  [self name], (self.isDirectory)?@"YES": @"NO"];
}
- (BOOL) isEmptyDirectory{
	if (self.isDirectory){
		
		NSFileManager *fm = [NSFileManager defaultManager];
		NSError *error = nil;
		NSArray *subfiles = [fm contentsOfDirectoryAtPath:[self fullName] error:&error];
		if (error) {
			NSLog(@"Error %s: %@", __func__, [error localizedDescription]);
		}
		if ([subfiles count])
			return NO;
		else
			return YES;
	}
	return NO;
}

#pragma mark -

- (void) unloadMembers{
	//TODO: release members and set them to nil
    _fullName = nil;
	_type = nil;
	_image = nil;
	_imageBig = nil;
	_creationDate = nil;
	_modificationDate = nil;
}

- (id) initWithName:(NSString *)aName parentDirectory:(NSString *)aParentDirectory{
	if (self = [super init]) {
		self.name = aName;
		self.parentDirectory = aParentDirectory;
		
		fullName = nil;
		type = nil;
		image = nil;
		imageBig = nil;
		creationDate = nil;
		modificationDate = nil;
		
	}
	return self;
}

- (id) init{
	return [self initWithName:@"" parentDirectory:@""];
}

-(void)dealloc
{
    NSLog(@"%s",__func__);
    [self unloadMembers];
}

#pragma mark -
#pragma mark NSCopying

- (id)copyWithZone:(NSZone *)zone{
	//Shallow copy : We need a shallow copies here since instances of N4File class 
	//will become keys in NSDictionaries as well and comparing pointers is easier much easier.
	return self;
}

@end
