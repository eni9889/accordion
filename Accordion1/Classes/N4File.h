//
//  File.h
//  MandalaChar
//
//  Created by Ignacio Enriquez Gutierrez on 8/9/10.
//  Copyright (c) 2010 Nacho4D.
//  See the file license.txt for copying permission.
//
#import <UIKit/UIKit.h>

@interface N4File : NSObject <NSCopying> {
	
	NSString *name;
	NSString *parentDirectory;
	
	NSString *fullName;
	NSString *type;
	
	UIImage *image;
	UIImage *imageBig;
	
	NSDate *creationDate;
	NSDate *modificationDate;
	
	BOOL expanded;
	NSInteger level; //used for cell indentation

}

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *parentDirectory;

@property (nonatomic, readonly, strong) NSString *fullName;
@property (nonatomic, readonly, strong) NSString *type;
@property (nonatomic, readonly, strong) NSDate *creationDate;
@property (nonatomic, readonly, strong) NSDate *modificationDate;

@property (nonatomic, readonly, strong) UIImage *image;
@property (nonatomic, readonly, strong) UIImage *imageBig;

@property (nonatomic, readonly, weak) NSString *detailText;


@property (nonatomic, readonly) BOOL isDirectory; 
@property (nonatomic, readonly) BOOL isEmptyDirectory;
@property (nonatomic, getter=isExpanded) BOOL expanded;
@property (nonatomic) NSInteger level;

- (id) initWithName:(NSString *)aName parentDirectory:(NSString *)aParentDirectory;

@end
