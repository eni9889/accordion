//
//  N4FileTreeCellView.m
//  Accordion
//
//  Created by Ignacio Enriquez Gutierrez on 8/28/10.
//  Copyright (c) 2010 Nacho4D.
//  See the file license.txt for copying permission.
//

#import "N4AccordionViewCell.h"
#import <QuartzCore/QuartzCore.h>

@implementation N4AccordionViewCell
@synthesize cellType, expanded;
@synthesize directoryAccessoryImageView;

- (void) setType:(N4AccordionViewCellType)newType
{
	cellType = newType;
	if (cellType == N4TableViewCellTypeFile )
    {
        directoryAccessoryImageView.image = nil;
    }
}

- (id) initWithReuseIdentifier:(NSString *)identifier
{
	if (self = [super initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:identifier]) {
		
		[self setupDirectoryImage];
		
		self.textLabel.textColor = [UIColor blackColor];
		self.detailTextLabel.textColor= [UIColor grayColor];
		
		cellType = N4TableViewCellTypeFile;
		expanded = NO;
	}
	return self;
}

-(void)prepareForReuse
{
    [super prepareForReuse];
    self.expanded = NO;
    self.directoryAccessoryImageView.transform = CGAffineTransformIdentity;
}

-(void)setupDirectoryImage
{
    self.directoryAccessoryImageView = nil;
    self.directoryAccessoryImageView = [[UIImageView alloc] init];
    self.directoryAccessoryImageView.contentMode = UIViewContentModeCenter;
    [self addSubview:directoryAccessoryImageView];
}

- (void) layoutSubviews
{
	[super layoutSubviews];
    
	float height = self.frame.size.height;
    float width = height;
	float indentation = self.indentationWidth*self.indentationLevel;
	
    float cellWidth = self.frame.size.width;
    float labelWidth = cellWidth - 2*height - indentation - 5;
    float labelOriginX = indentation + 2*width + 5;
    
    [directoryAccessoryImageView setFrame:CGRectMake(indentation, 0, width, height)];
    
	[self.imageView setFrame:CGRectMake(indentation + width, 0, width,height)];
	
    CGRect textLabelFrame = [[self textLabel] frame];
    textLabelFrame.origin.x = labelOriginX;
    textLabelFrame.size.width = labelWidth;
	[self.textLabel setFrame:textLabelFrame];
    
    CGRect detailTextLabelFrame = [[self detailTextLabel] frame];
    detailTextLabelFrame.origin.x = labelOriginX;
    detailTextLabelFrame.size.width = labelWidth;
	[self.detailTextLabel setFrame:detailTextLabelFrame];
    
}



- (void)setExpanded:(BOOL)flag{
	if (expanded != flag)
    {
		expanded = flag;
		
		if (expanded == YES)
        {
            [UIView setAnimationDuration:1.5f];
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationRepeatCount:1];
                self.directoryAccessoryImageView.transform = CGAffineTransformRotate(self.directoryAccessoryImageView.transform,M_PI/2);
            [UIView commitAnimations];
			
		}else{
			[UIView setAnimationDuration:1.5f];
            [UIView setAnimationBeginsFromCurrentState:YES];
            [UIView setAnimationRepeatCount:1];
                self.directoryAccessoryImageView.transform =  CGAffineTransformRotate(self.directoryAccessoryImageView.transform,-M_PI/2);
            [UIView commitAnimations];
		}
	}
}

@end
